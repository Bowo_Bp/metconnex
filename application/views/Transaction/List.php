<!DOCTYPE html>
<html>
<head>
    <title>List Visitor</title>
</head>
<body>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>List Visitor</h2>
            <?php echo (new \DateTime())->format('Y-m-d H:i:s:ms'); ?>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="<?php echo base_url('Transaction_Controller/create') ?>"> Create Registration</a>
        </div>
    </div>
</div>

<table class="table table-bordered">

  <thead>
      <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th width="220px">Action</th>
      </tr>
  </thead>

  <tbody>
   <?php foreach ($data as $item) { ?>     
    <?php $image = $item->VQR; ?>
    <?php $png = $image.'.png'; ?>
      <tr>
          <td><?php echo $item->VFIRSTNAME; ?></td>
          <td><?php echo $item->VLASTNAME; ?></td> 
          <td><img style="width: 100px;" src="<?php echo base_url().'assets/images/'.$image.'.png';?>"></td>         
      <td>
        <form method="DELETE" action="<?php echo base_url('Transaction/delete/'.$item->IID);?>">
          <a class="btn btn-info" href="<?php echo base_url('Transaction/'.$item->IID) ?>"> show</a>
         <a class="btn btn-primary" href="<?php echo base_url('Transaction/edit/'.$item->IID) ?>"> Edit</a>
          <button type="submit" class="btn btn-danger"> Delete</button>
        </form>
      </td>     
      </tr>
      <?php } ?>
  </tbody>
</table>
 </body>
</html>