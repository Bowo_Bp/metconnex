<!DOCTYPE html>
<html>
<head>
    <title>Add Visitor</title>
</head>
<body>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Visitor</h2>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add Visitor</h2>
        </div>
    </div>
</div>

<form method="post" action="<?php echo base_url('Transaction_Controller/store');?>">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>First Name:</strong>
                <input type="text" name="firstName" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Last Name:</strong>
                <input type="text" name="lastName" class="form-control">
            </div>
        </div>
        <input type="text" value="<?php echo (new \DateTime())->format('YmdHisms'); ?>" name="vqr" hidden="hidden">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>

    </div>


</form>
 </body>
</html>