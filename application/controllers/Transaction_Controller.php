<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_Controller extends CI_Controller {

	public $transactionCRUD;

		public function __construct() {
			parent::__construct(); 

			// $this->load->library('form_validation');
			// $this->load->library('session');
			$this->load->model('Transaction_Model');

			$this->transactionCRUD = new Transaction_Model;
		}

		public function index()
		{
				$data['data'] = $this->transactionCRUD->get_txnForm();

				//$this->load->view('theme/header');       
				$this->load->view('Transaction/List',$data);
				//$this->load->view('theme/footer');
		}

		public function show($id)
		{
				$item = $this->transactionCRUD->find_txnForm($id);


				//$this->load->view('theme/header');
				$this->load->view('Transaction/show',array('item'=>$item));
				//$this->load->view('theme/footer');
		}

		public function create()
		{
				//$this->load->view('theme/header');
				//$this->load->helper('form');
				$this->load->helper('url');
				$this->load->view('Transaction/Create');
				//$this->load->view('theme/footer');   
		}

		public function store()
		{
			// $this->form_validation->set_rules('VFIRSTNAME', 'FIRSTNAME', 'required');
			// $this->form_validation->set_rules('VLASTNAME', 'LASTNAME', 'required');

			// 		if ($this->form_validation->run() == FALSE){
			// 				$this->session->set_flashdata('errors', validation_errors());
			// 				redirect(base_url('transactionCRUD/create'));
			// 		}else{
			// 			$this->transactionCRUD->insert_txnForm();
			// 			redirect(base_url('transactionCRUD'));
			// 		}
			$this->transactionCRUD->insert_txnForm();
						redirect(base_url('Transaction_Controller/index'));
			}


		/**
			* Edit Data from this method.
			*
			* @return Response
		*/
		public function edit($id)
		{
				$item = $this->transactionCRUD->find_item($id);


				//$this->load->view('theme/header');
				$this->load->view('Transaction/edit',array('item'=>$item));
				//$this->load->view('theme/footer');
		}


		/**
			* Update Data from this method.
			*
			* @return Response
		*/
		public function update($id)
		{
					$this->form_validation->set_rules('VFIRSTNAME', 'FIRSTNAME', 'required');
					$this->form_validation->set_rules('VLASTNAME', 'LASTNAME', 'required');


					if ($this->form_validation->run() == FALSE){
							$this->session->set_flashdata('errors', validation_errors());
							redirect(base_url('Transaction/edit/'.$id));
					}else{ 
						$this->transactionCRUD->update_item($id);
						redirect(base_url('transactionCRUD'));
					}
		}


		/**
			* Delete Data from this method.
			*
			* @return Response
		*/
		public function delete($id)
		{
				$item = $this->transactionCRUD->delete_item($id);
				redirect(base_url('transactionCRUD'));
		}
}
