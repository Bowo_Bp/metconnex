<?php

class Transaction_Model extends CI_Model{


    public function get_txnForm(){
        // if(!empty($this->input->get("search"))){
        //   $this->db->like('VFIRSTNAME', $this->input->get("search"));
        //   $this->db->or_like('VLASTNAME', $this->input->get("search")); 
        // }
        $query = $this->db->get(" METCONNEX_TXNFORMS");
        return $query->result();
    }

    public function insert_txnForm()
    {    
        $vqr        = $this->input->post('vqr');
        $vfirstName = $this->input->post('firstName');
        $vlastName  = $this->input->post('lastName');

        $this->load->library('ciqrcode');

        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $qrName             = $vqr.'.png';
        $params['data']     = $vfirstName; //data yang akan di jadikan QR CODE
        $params['level']    = 'H'; //H=High
        $params['size']     = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$qrName; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        $data = array(
            'VQR'           => $vqr,
            'VFIRSTNAME'    => $vfirstName,
            'VLASTNAME'     => $vlastName
        );
        return $this->db->insert('METCONNEX_TXNFORMS', $data);
    }

    public function find_txnForm($id)
    {
        return $this->db->get_where('METCONNEX_TXNFORMS', array('IID' => $id))->row();
    }

    public function delete_txnForm($id)
    {
        return $this->db->delete('METCONNEX_TXNFORMS', array('IID' => $id));
    }
}
?>