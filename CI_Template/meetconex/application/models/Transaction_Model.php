<?php

class Transaction_Model extends CI_Model{

    public function insert_txnForm()
    {    
        $vqr            = $this->input->post('vqr');
        $vfirstName     = $this->input->post('firstName');
        $vlastName      = $this->input->post('lastName');
        $vemail         = $this->input->post('eMail');
        $vsalutation    = $this->input->post('salutation');
        $vjob           = $this->input->post('job');
        $vcompany       = $this->input->post('company');
        $vstreet        = $this->input->post('street');
        $vaddress2      = $this->input->post('address2');
        $vcity          = $this->input->post('city');
        $vregion        = $this->input->post('state');
        $vzipCode       = $this->input->post('zipCode');
        $vcountry       = $this->input->post('country');
        $vphone         = $this->input->post('phone');
        $vdesc          = $this->input->post('desc');
        $check_value    = implode(',', $this->input->post('chkForm'));

        $this->load->library('ciqrcode');

        $config['cacheable']    = true;
        $config['cachedir']     = './assets/';
        $config['errorlog']     = './assets/';
        $config['imagedir']     = './assets/img/qr/'; 
        $config['quality']      = true;
        $config['size']         = '1024';
        $config['black']        = array(224,255,255); 
        $config['white']        = array(70,130,180);
        $this->ciqrcode->initialize($config);

        $qrName             = $vqr.'.png';
        $params['data']     = $vfirstName;
        $params['level']    = 'H'; 
        $params['size']     = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$qrName; 
        $this->ciqrcode->generate($params); 
        $sendCode = FCPATH.$config['imagedir'].$qrName;;
        // Konfigurasi email
        $configMail = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'rezamuhammad.dev@gmail.com',    // email pengirim
            'smtp_pass' => '@Developer11',      // Password email pengirim
            'smtp_port' => 465,
            'crlf'      => "\r\n",
            'newline'   => "\r\n"
        ];

        $this->load->library('email', $configMail);

        $this->email->from('no-reply@JelajahWeb.com', 'JelajahWeb.com');

        $this->email->to($vemail); // email tujuan

        $this->email->attach($sendCode);

        $this->email->subject('Pendaftaran Event Jelajah Web');

        $this->email->message('Kami lampirkan file QR Code anda sebagai akses masuk ke Event yang kami selenggarakan, terima kasih sudah mendaftar.<br><br>Best regards,<br><strong>Jelajah Web</strong>');

        $data = array(
            'VQR'           => $vqr,
            'VFIRSTNAME'    => $vfirstName,
            'VLASTNAME'     => $vlastName,
            'VEMAIL'        => $vemail,
            'VSALUTATION'   => $vsalutation,
            'VPOSITION'     => $vjob,
            'VCOMPANNY'     => $vcompany,
            'VADDRESS1'     => $vstreet,
            'VADDRESS2'     => $vaddress2,
            'VCITY'         => $vcity,
            'VREGION'       => $vregion,
            'VSTATE'        => $vcountry,
            'VPOSTCODE'     => $vzipCode,
            'VPHONE'        => $vphone,
            'VDESC'         => $vdesc,
            'VRSTREGIST'    => $check_value
        );
        return $this->db->insert('METCONNEX_TXNFORMS', $data);
    }

}
?>