<footer>
        <div class="section-footer-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 logo-footer">
                        <img src="<?php echo base_url() ?>assets/img/logo-footer.jpg">
                        <br>
                        <br>
                        <ul class="socmed">
                            <li><a href=""><span class="fab fa-facebook-square"></span></a></li>
                            <li><a href=""><span class="fab fa-instagram"></span></a></li>
                            <li><a href=""><span class="fab fa-twitter-square"></span></a></li>
                            <li><a href=""><span class="fab fa-linkedin"></span></a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 menu-bottom">
                        <h5 class="font-white">Navigation</h5>
                        <ul class="menu-bottom-list">
                            <li><a href="">Home</a></li>
                            <li><a href="">About</a></li>
                            <li><a href="">Visiting</a></li>
                        </ul>
                        <ul class="menu-bottom-list">
                            <li><a href="">Exhibiting</a></li>
                            <li><a href="">Supporters</a></li>
                            <li><a href="">News</a></li>
                        </ul>
                    </div>
                    <div class="col-md-5 contact-list">
                        <h5 class="font-white">Contact</h5>
                        <ul class="listing-contact">
                            <li><i class="fas fa-phone-square"></i> <span>Leo Yulyardi </span> : 0812 8789 8957</li>
                            <li><i class="fas fa-phone-square"></i> <span>Prasasti Yudaprawira </span> : 0811 1826 153</li>
                            <li><i class="fas fa-phone-square"></i> <span>Anggira </span> : 0858 8139 9535</li>
                            <li><i class="fas fa-envelope-square"></i> <span>Email </span> 0812 8789 8957</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-footer-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-align-center">
                        <h6 class="font-white">Organized by:</h6>
                        <img src="<?php echo base_url() ?>assets/img/iai.png">
                    </div>
                    <div class="col-md-4 text-align-center">
                        <h6 class="font-white">Supported by:</h6>
                        <img src="<?php echo base_url() ?>assets/img/kementrian.png">
                        <img src="<?php echo base_url() ?>assets/img/logo-2.png">
                    </div>
                    <div class="col-md-4 text-align-center">
                        <h6 class="font-white">Co-Organized by:</h6>
                        <img src="<?php echo base_url() ?>assets/img/kupu.png">
                    </div>
                </div>
            </div>
        </div>
        <div class="section-footer-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-align-center">
                       <font color="#fff"> Copyrights © 2019 Metconnex. All rights reserved. </font>
                    </div>
                    
                </div>
                
            </div>
            
        </div>
    </footer>

</div>


    <!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>