<div class="container">
        <div class="countdown">
            <div class="row">
                <div class="col-md-6 offset-md-3 quote">
                   <h1> Enquiries</h1>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="clr"></div>
    <div class="section-3 pt30 pb30 text-align-center">
        <div class="container">
           
 <div class="form">
               
                <div class="form-content">
                    <div class="row">

                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Title *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Job Title *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Street Address *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address Line 2 *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="State / Province / Region *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Zip / Postal Code *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Country *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone Number *" value=""/>
                            </div>
                        </div> 
                        <div class="col-md-12">
                           <p align="left"> i am spesifically interested in finding out more about</p>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox"> Exhibiting
                            </label>
                        </div>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox"> Sponsoship
                            </label>
                        </div>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox"> Other
                            </label>
                        </div>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox"> Event Partnership
                            </label>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox"> Media Partnership
                            </label>
                        </div>
                        </div>
                        
                        <div class="col-md-12">
                            <p align="left">Which type of companies or people are you looking to discuss business with?</p>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="" value=""/>
                               <p align="left"> All information collected during this registration prosess is subject to our general Privacy policy which can be consulted here.</p>
                            </div>
                        </div> 
                        <div class="col-md-12">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox"> I have read, understood and consent to your Privacy policy.
                            </label>
                        </div>
                        </div>
     
                    </div>
                    <button type="button" class="btnSubmit">Submit</button>
                </div>
            </div>
        </div>
    </div>
    
   