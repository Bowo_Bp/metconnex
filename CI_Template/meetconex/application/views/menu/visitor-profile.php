

    <div class="container">
        <div class="countdown">
            <div class="row ">
                <div class="col-md-6 offset-md-3 quote ">
                    <div>
                    <h2 class="font-c"><b>Visitor Profile</b></h2>
                    <hr class="new2 text-align-center">
                </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="clr"></div>

    <div class="section-7  visitor  pt30 pb30 ">
        <div class="container"  >

<div class="container  ">
  <h3 class="text-align-center">INDONESIA’S MOST SENIOR MANUFACTURERS FROM THE FOLLOWING INDUSTRY SECTORS:</h3>
  <hr class="new2">
  <br><br>
  <div class="row">
    <div class="col-sm-4" >
          <div> <li class="fa fa-check"> </li>
             Construction equipment buyers
          </div>
          <div> <li class="fa fa-check"> </li> Importers & exporters, distributors & agents</div>
          <div><li class="fa fa-check"> </li>Architects </div>
          <div><li class="fa fa-check"> </li>Suppliers of aggregates</div>
          <div><li class="fa fa-check"> </li>Building & housing institutes and associations</div>
    </div>
    <div class="col-sm-4">
          <div> <li class="fa fa-check"> </li>Property & infrastructure developers</div>
          <div> <li class="fa fa-check"> </li>Civil and consulting engineers </div>
          <div><li class="fa fa-check"> </li>Contractors</div>
          <div><li class="fa fa-check"> </li>Private & public sector companies</div>
          <div><li class="fa fa-check"> </li>Land and quantity surveyors </div>
          <div><li class="fa fa-check"> </li>Decision makers</div>
    </div>
    <div class="col-sm-4">
          <div> <li class="fa fa-check"> </li>Building material producers</div>
          <div> <li class="fa fa-check"> </li>Construction & engineering companies</div>
          <div><li class="fa fa-check"> </li>Planners </div>
          <div><li class="fa fa-check"> </li>Building merchants</div>
          <div><li class="fa fa-check"> </li>Finance & leasing construction equipment companies</div>
    </div>

  </div>
</div>
        
     
        </div>
        </div>
    </div>
    
  