

    <div class="container">
        <div class="countdown">
            <div class="row ">
                <div class="col-md-6 offset-md-3 quote ">
                    <div>
                    <h3 class="font-c"><b>Why Visit ?</b></h3>
                    <hr class="new2 text-align-center">
                </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="clr"></div>

<div class="section-black pt30 pb30">
<div class="container  ">
  <!-- <h4 class="text-left">Why Exhibit ?</h4>
  <hr class="new3"> -->
  <font color="#b3b0b0">Join thousands of fellow professionals at the Construction Indonesia. With guaranteed face to face networking and business opportunities as well as the chance to hear from industry experts in our in-depth seminar programme, Construction Indonesia offers the ultimate solution to helping you achieve your business objectives.</font>
</div>        
        </div>

<div class="section-grey pt30 pb30">
  <div class="container">
    <div class="row">

      <div class="col-sm-4" >
         
          <img src="<?php echo base_url() ?>assets/img/WhyVisit_1.png" width="100%">
         
        </div>
        <div class="col-sm-4" >
          
          <img src="<?php echo base_url() ?>assets/img/WhyVisit_2.png" width="100%">
          
        </div>
        <div class="col-sm-4" >
          
          <img src="<?php echo base_url() ?>assets/img/WhyVisit_3.png" width="100%">
         
        </div>
        
    </div>
    <br>
    <br>
    <div class=" container">
      <p><strong>Key benefits of visiting</strong>
      <ul>
        <li>Take your business to the next level: Be inspired and gather solutions and new ideas from our exhibitors, seminar and networking opportunities.</li>
        <li>The only dedicated platform meeting the needs of the entire construction industry</li>
        <li>Discover new products and services and grow your business</li>
        <li>Network: Get connected and build new business relationships</li>
        <li>Experience Indonesia: An exciting and developing destination</li>
        <br>
        <p>The show will feature a large indoor and outdoor exhibition, an international pavilion, live demonstrations and a high-level industry seminar. More than 228 companies will exhibit from industry sectors including heavy equipment, Tools and Equipment, Building Materials, Infrastructure, Manpower and Management, and more!
        
      </ul>

    </div>
  </div>
  
</div>        
        </div>
    </div>
    
  