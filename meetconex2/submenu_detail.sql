-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2019 at 12:46 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meetconex`
--

-- --------------------------------------------------------

--
-- Table structure for table `submenu_detail`
--

CREATE TABLE `submenu_detail` (
  `id` int(11) NOT NULL,
  `submenu_id` int(11) NOT NULL,
  `submenu_det` varchar(60) NOT NULL,
  `sublink` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submenu_detail`
--

INSERT INTO `submenu_detail` (`id`, `submenu_id`, `submenu_det`, `sublink`) VALUES
(1, 10, 'tes', 'tes'),
(2, 10, 'About IAMET ITB', ''),
(3, 10, 'About The Show', ''),
(4, 10, 'Dates & Venue', ''),
(5, 11, 'Why	Visit?', ''),
(6, 11, 'Show Features', ''),
(7, 11, 'Conference', ''),
(8, 11, ' Exhibitors Preview', ''),
(9, 11, ' EPlan Your Visit', ''),
(10, 11, ' Dates & Venue', ''),
(11, 11, ' Visitor Profile', ''),
(12, 11, ' Official Hotel', ''),
(13, 11, ' Visa Informationt', ''),
(14, 11, ' Free Visitor	Registration', ''),
(15, 12, ' Agenda', ''),
(16, 12, ' Speaker Profile', ''),
(17, 12, ' Delegate	Registration', ''),
(18, 13, 'Why Exhibit', ''),
(19, 13, 'Exhibitor	Enquiries', ''),
(20, 13, 'Exhibit Profile', ''),
(21, 13, 'Sales	Brochure', ''),
(22, 14, 'Why Sponsor?', ''),
(23, 14, 'Sponsorship Opportunity', ''),
(24, 14, 'Sponsorship Enquiry', ''),
(25, 14, 'Download Sponsorship Brochure', ''),
(26, 15, 'Press Corner', ''),
(27, 15, 'Latest	News', ''),
(28, 15, 'Gallery', ''),
(32, 16, 'Contact The Organisers', ''),
(33, 16, 'Enquiries', ''),
(34, 17, 'REGISTRATION', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `submenu_detail`
--
ALTER TABLE `submenu_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `submenu_detail`
--
ALTER TABLE `submenu_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
