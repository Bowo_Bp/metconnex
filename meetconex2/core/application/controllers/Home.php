<?php 
	class Home extends MY_Controller{
		
	function __construct(){
		parent::__construct();
		/*get Menu model*/		
		$this->load->model('menu_m');
		/*get Menu model*/

		/*get Content data Model*/
		$this->load->model('content_m');
		/*get Content data Model*/
	}
		/*Dashboard*/
		public function index(){
 
			$data['menu'] = $this->menu_m->tampil();
			$data['title']='Dashboard';
			$this->load->template('backend/form/index',$data);
		}
		/*Dashboard*/


/*--------------------------------Contact The Organisers-------------------------------------------*/
		/*List Data*/
	       public function contact_list(){
	       		$data['menu'] = $this->menu_m->tampil();
	       		$data['title']='contact organizers';
	       		$data['contact'] = $this->content_m->list_contact('contact_organizers')->result();
				$this->load->template('backend/contact/list',$data);
		}
		/*List Data*/

		/*Input*/
	       public function contact_form(){
				$data['menu'] = $this->menu_m->tampil();
				$data['title']='Contact The Organisers';
				$this->load->template('backend/contact/form',$data);
		}
		
			public function contact_insert(){

				$name = $this->input->post('name');
				$phone = $this->input->post('phone');
				$email = $this->input->post('email');
		 
				$data = array(
					'name' => $name,
					'phone' => $phone,
					'email' => $email
					);
				$this->content_m->input_data($data,'contact_organizers');
				redirect('home/contact_list');
		}
		/*Input*/

/*------------------------------------Edit----------------------------------*/
		/*Edit*/
		function contact_edit($id){
				$data['menu'] = $this->menu_m->tampil();
				$data['title']='Edit Organisers';
				$where = array('id' => $id);
				$data['contact'] = $this->content_m->edit_data($where,'contact_organizers')->result();
				$this->load->view('backend/contact/form_edit',$data);
		/*Edit*/
	}

		/*Proses Edit 1*/
		function update_contact(){
			$id = $this->input->post('id');
			$name = $this->input->post('name');
			$phone = $this->input->post('phone');
			$email = $this->input->post('email');
		 
			$data = array(
				'name' => $name,
				'phone' => $phone,
				'email' => $email
			);
		 
			$where = array(
				'id' => $id
			);
		 
			$this->content_m->update_data($where,$data,'contact_organizers');
			//redirect('crud/index');
			redirect('home/contact_list');
		}
		/*Proses Edit*/
/*------------------------------------Edit----------------------------------*/
		/*Delete*/
		function contact_delete($id){
				$where = array('id' => $id);
				$this->content_m->delete_data($where,'contact_organizers');
				redirect('home/contact_list');
		/*Delete*/
	}

/*--------------------------------Contact The Organisers-------------------------------------------*/


	}












