<!DOCTYPE html>
<html lang="en">
<head>        
    <title><?php echo $title; ?></title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="<?php echo base_url() ?>assets/css/stylesheets.css" rel="stylesheet" type="text/css" />        
    
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/jquery/jquery.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/jquery/jquery-ui.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/jquery/globalize.js'></script>    
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/bootstrap/bootstrap.min.js'></script>

        <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/uniform/jquery.uniform.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.js'></script>
    
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/uniform/jquery.uniform.min.js'></script>
    
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/knob/jquery.knob.js'></script>
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/sparkline/jquery.sparkline.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.js'></script>     
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.resize.js'></script>



    
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins.js'></script>    
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/actions.js'></script>    
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/charts.js'></script>
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/settings.js'></script>
    
</head>
<body class="bg-img-num1" data-settings="open"> 
    
    <div class="container">        
        <div class="row">                   
            <div class="col-md-12">
                
                 <nav class="navbar brb" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-reorder"></span>                            
                        </button>                                                
                        <a class="navbar-brand" href="index.php"><img src="<?php echo base_url() ?>assets/img/logo.png"/></a>                                                                                     
                    </div>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">



        <ul class="nav navbar-nav">
 
            <?php 
 
                foreach ($menu as $menus) { 

            
                    $a = $menus->id;
                    $submenu = $this->db->query("SELECT * FROM sub_menu WHERE menu_id = '$a' ");
                    if ($submenu->num_rows() > 0) {

                           echo '<li class="dropdown"><a href="'. site_url($menus->url) .'" class="dropdown-toggle" data-toggle="dropdown">'. $menus->menu_utama .'</a>';
            
   
                            echo '<ul class="dropdown-menu">';
                             foreach ($submenu->result() as $submenus) {
                     $b = $submenus->id;
                    $submenu2 = $this->db->query("SELECT * FROM submenu_detail WHERE submenu_id = '$b' ");
                            if ($submenu2->num_rows() == 0) {
                                echo '<li><a href="'. site_url($submenus->submenu_link) .'">'.$submenus->submenu.'</a>';
                                echo '</li>';
                            }
                            else{ 
                               echo '<li><a href="'. site_url($submenus->submenu_link) .'"><i class="icon-angle-right pull-right"></i>'.$submenus->submenu.'</a>';
                               echo '<ul class="dropdown-submenu">';
                               foreach($submenu2->result() as $submenus2){
                                
                                ?>
                                        
                                            <li><a href="email_sample_1.html"><?php echo $submenus2->submenu_det; ?></a></li>
                                       
                                        
                                   
                            <?php }
                             echo ' </ul></li>';}
                           
                            }
                            echo "</ul></li>";
                        }
                    else{
                       echo '<li class="active">';
                   echo '<a href="'. site_url($menus->url) .'">'. $menus->menu_utama .'</a> </li>'; 
                };
                
             } ?>
 
            </ul>


                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="search..."/>
                            </div>                            
                        </form>                                            
                    </div>
                </nav>                

            </div>            
        </div>