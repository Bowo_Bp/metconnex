<!DOCTYPE html>
<html lang="en">
<head>        
    <title>Taurus</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="css/stylesheets.css" rel="stylesheet" type="text/css" />        
    
    <script type='text/javascript' src='js/plugins/jquery/jquery.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/jquery-ui.min.js'></script>   
    <script type='text/javascript' src='js/plugins/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/globalize.js'></script>    
    <script type='text/javascript' src='js/plugins/bootstrap/bootstrap.min.js'></script>
    
    <script type='text/javascript' src='js/plugins/uniform/jquery.uniform.min.js'></script>
    <script type='text/javascript' src='js/plugins/datatables/jquery.dataTables.min.js'></script>
    
    <script type='text/javascript' src='js/plugins.js'></script>    
    <script type='text/javascript' src='js/actions.js'></script>
    <script type='text/javascript' src='js/settings.js'></script>
</head>
<body class="bg-img-num1"> 
    
    <div class="container">        
        <div class="row">                   
            <div class="col-md-12">
                
                 <nav class="navbar brb" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-reorder"></span>                            
                        </button>                                                
                        <a class="navbar-brand" href="index.html"><img src="img/logo.png"/></a>                                                                                     
                    </div>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">                                     
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="index.html">
                                    <span class="icon-home"></span> dashboard
                                </a>
                            </li>                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-edit"></span> Artikel</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="list_artikel.php">Semua Artikel</a></li>
                                    <li><a href="form_editors.html">Tambah Artikel</a></li>
                                    
                                </ul>                                
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-reorder"></span> Kategori</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">Semua kategori</a></li>
                                    <li><a href="form_editors.html">TAmbah Kategori</a></li>
                                </ul>                                
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-calendar"></span> Agenda</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">Semua Agenda</a></li>
                                    <li><a href="form_editors.html">TAmbah Agenda</a></li>
                                </ul>                                
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-download-alt"></span> Download</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">List Download</a></li>
                                    <li><a href="form_editors.html">Tambah file</a></li>
                                </ul>                                
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-picture"></span> Gallery</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">List Gallery</a></li>
                                    <li><a href="form_editors.html">Tambah Gallery</a></li>
                                </ul>                                
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-tags"></span> Menu</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">Form elements</a></li>
                                </ul>                                
                            </li>
                           <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-user"></span> User</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">Managemen User</a></li>
                                    <li><a href="form_editors.html">User Baru</a></li>
                                    <li><a href="form_files.html">Profile</a></li>
                                </ul>                                
                            </li>
                           <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-cogs"></span> General</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">Informasi Web</a></li>
                                </ul>                                
                            </li>                           
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="search..."/>
                            </div>                            
                        </form>                                            
                    </div>
                </nav>                

            </div>            
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li><a href="#">Components</a></li>                                        
                    <li><a href="#">Tables</a></li>                                        
                    <li class="active">List Agenda</li>
                </ol>
            </div>
        </div> 
        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="block">
                    <div class="header">
                        <h2>List Agenda</h2>                                        
                    </div>
                    <div class="content">

                        <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkall"/></th>
                                    <th width="25%">Judul Agenda</th>
                                    <th width="15%">Tanggal</th>
                                    <th width="10%">Berakhir</th>  
                                    <th width="25%">Action</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>101</td>
                                    <td>Dmitry</td>
                                    <td>dmitry@domain.com</td>
                                    <td>+98(765)432-10-98</td>
                             
                                                            
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>102</td>
                                    <td>Alex</td>
                                    <td>alex@domain.com</td>
                                    <td>+98(765)432-10-99</td> 
                     
                                                          
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>103</td>
                                    <td>John</td>
                                    <td>john@domain.com</td>
                                    <td>+98(765)432-10-97</td>  
                               
                                                                  
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>104</td>
                                    <td>Angelina</td>
                                    <td>angelina@domain.com</td>
                                    <td>+98(765)432-10-90</td> 
                             
                                                                   
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>105</td>
                                    <td>Tom</td>
                                    <td>tom@domain.com</td>
                                    <td>+98(765)432-10-92</td> 
                               
                                                                
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>106</td>
                                    <td>Helen</td>
                                    <td>helen@domain.com</td>
                                    <td>+98(765)432-11-33</td> 
                               
                                                              
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>107</td>
                                    <td>Aqvatarius</td>
                                    <td>aqvatarius@domain.com</td>
                                    <td>+98(765)432-15-66</td>  
                                 
                                                          
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>108</td>
                                    <td>Olga</td>
                                    <td>olga@domain.com</td>
                                    <td>+98(765)432-11-97</td> 
                                 
                                                                
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>109</td>
                                    <td>Homer</td>
                                    <td>homer@domain.com</td>
                                    <td>+98(765)432-11-90</td> 
                                                    
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>110</td>
                                    <td>Tifany</td>
                                    <td>tifany@domain.com</td>
                                    <td>+98(765)432-11-92</td> 
                             
                                                                 
                                </tr>                                
                            </tbody>
                        </table>                                        

                    </div>
                </div>
          
            </div>
            
        </div>
        
    </div>
    
</body>
</html>