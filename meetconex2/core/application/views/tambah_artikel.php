<!DOCTYPE html>
<html lang="en">
<head>        
    <title>Taurus</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="js/plugins/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css" rel="stylesheet" type="text/css" />
    <link href="css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="css/codemirror/theme/midnight.css" rel="stylesheet" type="text/css" />
    
    <script type='text/javascript' src='js/plugins/jquery/jquery.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/jquery-ui.min.js'></script>   
    <script type='text/javascript' src='js/plugins/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/globalize.js'></script>    
    <script type='text/javascript' src='js/plugins/bootstrap/bootstrap.min.js'></script>
    
    <script type='text/javascript' src='js/plugins/cleditor/jquery.cleditor.min.js'></script>
    
    <script type='text/javascript' src='js/plugins/codemirror/codemirror.js'></script>
    <script type='text/javascript' src="js/plugins/codemirror/addon/edit/matchbrackets.js"></script>
    <script type='text/javascript' src="js/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script type='text/javascript' src="js/plugins/codemirror/mode/xml/xml.js"></script>
    <script type='text/javascript' src="js/plugins/codemirror/mode/javascript/javascript.js"></script>
    <script type='text/javascript' src="js/plugins/codemirror/mode/css/css.js"></script>
    <script type='text/javascript' src="js/plugins/codemirror/mode/clike/clike.js"></script>
    <script type='text/javascript' src="js/plugins/codemirror/mode/php/php.js"></script>    
    
    <script type='text/javascript' src='js/plugins/uniform/jquery.uniform.min.js'></script>
    
    <script type='text/javascript' src='js/plugins/tinymce/tinymce.min.js'></script>
    
    <script type='text/javascript' src='js/plugins.js'></script>    
    <script type='text/javascript' src='js/actions.js'></script>
    <script type='text/javascript' src='js/settings.js'></script>
</head>
<body class="bg-img-num1"> 
    
    <div class="container">        
        <div class="row">                   
            <div class="col-md-12">
                
                 <nav class="navbar brb" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-reorder"></span>                            
                        </button>                                                
                        <a class="navbar-brand" href="index.html"><img src="img/logo.png"/></a>                                                                                     
                    </div>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">                                     
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="index.html">
                                    <span class="icon-home"></span> dashboard
                                </a>
                            </li>                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-edit"></span> Artikel</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="list_artikel.php">Semua Artikel</a></li>
                                    <li><a href="form_editors.html">Tambah Artikel</a></li>
                                    
                                </ul>                                
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-reorder"></span> Kategori</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">Semua kategori</a></li>
                                    <li><a href="form_editors.html">TAmbah Kategori</a></li>
                                </ul>                                
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-calendar"></span> Agenda</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">Semua Agenda</a></li>
                                    <li><a href="form_editors.html">TAmbah Agenda</a></li>
                                </ul>                                
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-download-alt"></span> Download</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">List Download</a></li>
                                    <li><a href="form_editors.html">Tambah file</a></li>
                                </ul>                                
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-picture"></span> Gallery</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">List Gallery</a></li>
                                    <li><a href="form_editors.html">Tambah Gallery</a></li>
                                </ul>                                
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-tags"></span> Menu</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">Form elements</a></li>
                                </ul>                                
                            </li>
                           <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-user"></span> User</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">Managemen User</a></li>
                                    <li><a href="form_editors.html">User Baru</a></li>
                                    <li><a href="form_files.html">Profile</a></li>
                                </ul>                                
                            </li>
                           <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-cogs"></span> General</a>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="form_elements.html">Informasi Web</a></li>
                                </ul>                                
                            </li>                           
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="search..."/>
                            </div>                            
                        </form>                                            
                    </div>
                </nav>                

            </div>            
        </div>

        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li><a href="#">Components</a></li>                    
                    <li class="active">Tambah Artikel</li>
                </ol>
            </div>
        </div>        
        
       
        
        <div class="row">
            <div class="col-md-8">
                
                <div class="block block-fill-white">
                    <div class="header">
                        <h2>Text Editor</h2>
                    </div>                    
                    <div class="content np">
                        <textarea class="cle">
                        <h2>Lorem ipsum dolor sit amet</h2>
                        <p><strong>Consectetur adipiscing</strong> elit. Sed dictum dolor sem, quis pharetra dui ultricies vel. Cras non pulvinar tellus, vel bibendum magna. Morbi tellus nulla, cursus non nisi sed, porttitor dignissim erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc facilisis commodo lectus. Vivamus vel tincidunt enim, non vulputate ipsum. Ut pellentesque consectetur arcu sit amet scelerisque. Fusce commodo leo eros, ut eleifend massa congue at.</p>
                        <p>Nam a nisi et nisi tristique lacinia non sit amet orci. <strong>Duis blandit leo</strong> odio, eu varius nulla fringilla adipiscing. Praesent posuere blandit diam, sit amet suscipit justo consequat sed. Duis cursus volutpat ante at convallis. Integer posuere a enim eget imperdiet. Nulla consequat dui quis purus molestie fermentum. Donec faucibus sapien eu nisl placerat auctor. Pellentesque quis justo lobortis, tempor sapien vitae, dictum orci.</p>
                        </textarea>
                    </div>
                    <div class="footer">
                        <div class="pull-right">
                            <button class="btn btn-success">Submit</button>
                        </div>
                    </div>                    
                </div>
                    
            </div>
                      
        </div>        
        
                                
        
    </div>
    
</body>
</html>