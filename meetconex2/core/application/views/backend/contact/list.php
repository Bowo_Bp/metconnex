  <div class="row">  
            <div class="col-md-4">
                <div class="block">
                    <div class="content controls">
                        <div class="form-row">
                         <div class="col-md-12"><a class=" col-md-12 btn btn-default btn-save col-md-12" href="<?php echo base_url() ?>home/contact_list" /> List Contact</a></div>
                        </div>
                       
                        <div class="form-row">
                         <div class="col-md-12"><a class=" col-md-12 btn btn-default btn-save col-md-12" href="<?php echo base_url() ?>home/contact_form" /> Tambah Contact</a></div>
                        </div>
                    </div>
                
                </div>
            </div>

            <div class="col-md-8">
                <div class="block">
                    <div class="header">
                        <h2>Contact Organizers</h2>                                        
                    </div>
                    <div class="content">

                        <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable_simple">
                            <thead>
                                <tr>
                                    <th >Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Action</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($contact as $contacts){ ?>
                                <tr>
                                    <td><?php echo $contacts->name;?> </td>
                                    <td><?php echo $contacts->phone;?> </td>
                                    <td><?php echo $contacts->email;?> </td>
                                    <td>
                                    <a href="<?php echo site_url();?>home/contact_edit/<?php echo $contacts->id;?>"><i class="icon-edit"></i> </a>
                                     <a href="<?php echo site_url();?>home/contact_delete/<?php echo $contacts->id;?>"><i class="icon-trash"></i> </a>
                                    </td>                                    
                                </tr>
                            <?php } ?>
                                                          
                            </tbody>
                        </table>                                        

                    </div>
                </div>
          
            </div>
            
        </div>
        
  