<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Met Connect</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font.css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
    function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

    
</head>
<body>
<div class="section-1">
    <div class="container header-home">
        <div class="row">
            <div class="col-md-3 logo-top"><a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>assets/img/logo.jpg"></a></div>
            <div class="col-md-7 tgl-top">
                <h3>7 AUG 2019 - 9 AUG 2019</h3>
                <p>Jakarta Convention Centre, Senayan Jakarta</p>
            </div>
            <div class="col-md-2 pt30">
            <a class="btn-orange-1" href="<?php echo base_url() ?>Main_menu/registrationQuiries" >Registration</a>
            </div>
        </div>
    </div>

<!-- Navbar -->

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <!-- <a class="nav-item nav-link active" href="<?php echo base_url() ?>">Home <span class="sr-only">(current)</span></a> -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          About
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="#">About	IAMET	ITB</a>
          <a class="dropdown-item" href="<?php echo base_url()?>Main_menu/AboutTheShow">About The Show</a>
          <a class="dropdown-item" href="#">The Venue</a>
   
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Visiting
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Why Visit ?</a>
          <a class="dropdown-item" href="#">Show Features</a>
          <a class="dropdown-item" href="<?php echo base_url()?>Main_menu/exhibitors_preview">Exhibitors Preview</a>
          <a class="dropdown-item" href="#">Where & When</a>
          <a class="dropdown-item" href="<?php echo base_url()?>Main_menu/Visitor_Profile">Visitor Profile</a>
          <a class="dropdown-item" href="<?php echo base_url()?>Main_menu/official_hotel">Official Hotel</a>
          <a class="dropdown-item" href="<?php echo base_url()?>Main_menu/Visa_Information">Visa Information</a>
          <a class="dropdown-item" href="#">Free Visitor Registration</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Exhibiting
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url()?>Main_menu/why_exhibit">Why Exhibit</a>
          <a class="dropdown-item" href="<?php echo base_url()?>Main_menu/exhibith_encuiries">Exhibitor & Sponsorship Enquiries</a>
          <a class="dropdown-item" href="#">Exhibit Profile</a>
          <a class="dropdown-item" href="#">Sales Brochure</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Supporters
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Media Partners</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          News
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Latest News</a>
          <a class="dropdown-item" href="#">Gallery</a>
         
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Contact
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url() ?>Main_menu/contact_organizer">Contact The Organizers</a>
          <a class="dropdown-item" href="<?php echo base_url() ?>Main_menu/registration">Enquiries</a>
        </div>
      </li>


      
    </div>
  </div>
 
</nav>

<!-- End Navbar-->


