<style>
    {
  box-sizing: border-box;
}

body {
  background-color: #f1f1f1;
}

#regForm {
  background-color: #ffffff;
  margin: 100px auto;
  font-family: Raleway;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}
</style>

<div class="container">
        <div class="countdown">
            <div class="row ">
                <div class="col-md-6 offset-md-3 quote ">
                    <div>
                    <h3 class="font-c"><b> Visitor Registration</b></h3>
                    <hr class="new2 text-align-center">
                </div>
                </div>
            </div>
            
        </div>
    </div>

<div class="clr"></div>
    <div class="section-3 pt30 pb30 text-align-center">
        <div class="container">
       
 <div class="form">
        
        <form method="post" action="<?php echo base_url('Transaction/create');?>">
                <!-- <h1>Register:</h1> -->
                <div class="tab"> 
                    <h3 style=" float:left">Title:</h3>
                    <!-- <p><input type="text" class="form-control" placeholder="Title *" value="" name="salutation"/></p> -->
                    <p><select name="salutation" class=" form-control">
                      <option value="Mr">Mr</option>
                      <option value="Mr">Mrs</option>
                      <option value="Mr">Miss</option>
                      <option value="Mr">Ms</option>
                    </select></p>
                    <p><input type="text" class="form-control" placeholder="Job Title *" value="" name="job"/></p>
                    <p><input type="text" class="form-control" placeholder="Company Name *" value="" name="company"/></p>
                </div>
                <div class="tab"> 
                    <h3 style=" float:left">Name:</h3>
                    <p><input type="text" class="form-control" placeholder="First Name *" value="" name="firstName"/></p>
                    <p><input type="text" class="form-control" placeholder="Last Name *" value="" name="lastName"/></p>
                </div>
                <div class="tab">
                <h3 style=" float:left">Contact Info:</h3>
                    <p><input type="text" class="form-control" placeholder="Street Address *" value="" name="street"/></p>
                    <p><input type="text" class="form-control" placeholder="Address Line 2 *" value="" name="address2"/></p>
                    <p><input type="text" class="form-control" placeholder="City *" value="" name="city"/></p>
                    <p><input type="text" class="form-control" placeholder="State / Province / Region *" value="" name="state"/></p>
                    <p><input type="text" class="form-control" placeholder="Zip / Postal Code *" value="" name="zipCode"/></p>
                    <p><input type="text" class="form-control" placeholder="Country *" value="" name="country"/></p>
                    <p><input type="text" class="form-control" placeholder="Email *" value="" name="eMail"></p>
                    <p><input type="text" class="form-control" placeholder="Phone Number *" value="" name="phone"></p>
                </div>
                <input type="text" value="<?php echo (new \DateTime())->format('YmdHisms'); ?>" name="vqr" hidden="hidden">
                <div style="overflow:auto;">
                    <div style="float:right;">
                    <button type="button"  class="btnPrev" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                    <button type="button" class="btnNext" id="nextBtn" onclick="nextPrev(1)">Next</button>
                    </div>
                </div>
                <!-- Circles which indicates the steps of the form: -->
                <div style="text-align:center;margin-top:40px;">
                    <span class="step" style="background-color: #f8c411;"></span>
                    <span class="step" style="background-color: #f8c411;"></span>
                    <span class="step" style="background-color: #f8c411;"></span>
                    <span class="step" style="background-color: #f8c411;"></span>
                </div>
                </form>
            </div>
        </div>
    </div>

<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>
    
   