

    <div class="container">
        <div class="countdown">
            <div class="row ">
                <div class="col-md-6 offset-md-3 quote ">
                    <div>
                    <h2 class="font-c"><b>Why Sponsor ?</b></h2>
                    <hr class="new2 text-align-center">
                </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="clr"></div>

    <div class="section-7  visitor  pt30 pb30 ">
        <div class="container"  >

<div class="container  ">

  <div class="row">
    <div class="col-sm-12">

    <div><b>Broker new business partnerships</b></div>
    <div>Broker new business partnerships
        MET CONNEX 2019 presents you with an ideal opportunity to attract qualified decision makers and connect with new leads.</div>
    <p></p>
    </div>
    
    <div class="col-sm-12">
    <div><b>Promote thought leadership</b></div>
    <div>Position yourself as an industry expert and gain credibility in front of key industry buyers</div>
    <p></p>
    </div>
    
    <div class="col-sm-12">
    <div><b>Government credibility</b></div>
    <div>Sponsoring the event will show extended support for the national initiative and will strengthen your relationships with key government departments</div>
    <p></p>
    </div>

    <div class="col-sm-12">
    <div><b>Enhance your corporate brand and image</b></div>
    <div>Showcasing your company establishes you, in the eyes of your prospective or existing clients, as a market-leading brand creating a strong image of your company’s capabilities.</div>
    <p></p>
    </div>

    <div class="col-sm-12">
    <div><b>Build relationships with the media</b></div>
    <div>MET CONNEX 2019 runs with the support of trade press and industry journalists. Opportunities for editorial coverage, interviews and developing better relations can be crucial to your company’s success and image.</div>
    <p></p>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12" >
      <img src="<?php echo base_url() ?>assets/img/whysponsor.png">
    </div>
   </div> 



</div>
</div>
</div>
        
     
        </div>
        </div>
    </div>
    
  