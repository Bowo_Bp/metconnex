

    <div class="container">
        <div class="countdown">
            <div class="row ">
                <div class="col-md-6 offset-md-3 quote ">
                    <div>
                    <h2 class="font-c"><b>Why Visit?</b></h2>
                    <hr class="new2 text-align-center">
                </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="clr"></div>

    <div class="section-7  visitor  pt30 pb30 ">
        <div class="container"  >

<div class="container  ">

  <div class="row">
    <div class="col-sm-12" >
      <p>
Join thousands of fellow professionals at the METALLURGY CONFERENCE & EXPO 2019. With guaranteed face to face networking and business opportunities as well as the chance to hear from industry experts in our in-depth conference programme, METALLURGY CONFERENCE & EXPO 2019 offers the ultimate solution to helping you achieve your business objectives.
      </p>
      <p>Key benefits of visiting</p>
      <ul>
      <li>Take your business to the next level: Be inspired and gather solutions and new ideas from our exhibitors, conference and networking opportunities.</li>
      <li>The only dedicated platform meeting the needs of the entire metallurgy industry.</li>
      <li>Discover new products and services and grow your business, through hundreds of international exhibitors who will showcase the latest technologies and solutions for your business.</li>
      <li>Meet new and existing suppliers, to discuss business requirements with new and established partners.</li>
      <li>Networking – maintain your relation and create new business opportunity with international industry professionals and stakeholders.</li>
      <li>Keep up-to-date with the latest trends and policies, developments and opportunities across the industry.</li>
      <li>Gain more knowledge with our unrivaled education programmes exclusive to METALLURGY CONFERENCE & EXPO 2019.</li>
      <li>Experience Indonesia: An exciting and developing destination.</li>
    </ul>
    <p>The show will feature a large indoor exhibition, an international pavilion and a high-level industry conference. More than hundred companies will exhibit from Mining Company, Metallurgical Processing Company B2B, Supporting Products Company, Technology Vendor, Engineering Company and more!</p>
    </div>
   </div> 




  </div>
</div>
        
     
        </div>
        </div>
    </div>
    
  