

    <div class="container">
        <div class="countdown">
            <div class="row ">
                <div class="col-md-6 offset-md-3 quote ">
                    <div>
                    <h2 class="font-c"><b>Date and Vanue</b></h2>
                    <hr class="new2 text-align-center">
                </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="clr"></div>

    <div class="section-7  visitor  pt30 pb30 ">
        <div class="container"  >

<div class="container  ">

  <div class="row">
    <div class="col-sm-12" >
      <h4>Dates</h4>
      <p>
        <b>METALLURGY CONFERENCE & EXPO 2019</b> will be held on 7 - 9 August 2019<br>
        Opening time: 
      </p>
    </div>
   </div> 
   <div class="row">
         <div class="col-sm-4">
          7 August<br>
          Opening time  : 11.00<br>
          Closing time    : 18.00<br>     
      </div>
      <div class="col-sm-4">
          8 August<br>
          Opening time  : 09.00<br> 
         Closing time    : 18.00<br>
      </div>
      <div class="col-sm-4">
          9 August<br>
          Opening time  : 09.00 <br>
          Closing time    : 18.00<br>
      </div>
    </div>
    <br><br>
  <div class="row">
    <div class="col-sm-12" >
      <h4>About The Venue</h4>
      <p>
            The Jakarta Convention Center or JCC (Indonesian: Balai Sidang Jakarta) is located in Gelora Bung Karno Sports Complex, Tanah Abang, Central Jakarta, Indonesia. It has a plenary hall that has 5,000 seats, JCC also has an assembly hall with an area of 3,921 m². JCC has 13 various sized meeting rooms. JCC is connected to The Sultan Hotel & Residence Jakarta (formerly Jakarta Hilton International) by an underground tunnel. The tunnel has moving walkways and is air conditioned.
            Jakarta Convention Center complex was completed in 1974 for the opening ceremony of the 23rd annual Pacific Asia Travel Association conference that was held in early April 1974. The conference was a major event for Jakarta and several large hotel projects, such as Hotel Indonesia extension, Hotel Borobudur, Hotel Ambassador (now Hotel Aryaduta), and Hotel Sahid Jaya, were also targeted for completion before the PATA conference began.

      </p>
    </div>
   </div> 

  </div>


  </div>
</div>
        
     
        </div>
        </div>
    </div>
    
  