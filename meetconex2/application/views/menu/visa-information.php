<div class="col-md-8">

<div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Visa-On-Arrival (VOA)
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        The Pay for Visa-On-arrival (VOA) system applies for citizens of certain nations.

Please visit the Indonesian Embassy website for your country, to see the full list of nations and which international gateways you can require the visa from.

The cost of the 30-day (only) tourist visa is US$35/person for a 30-day visa and US$10/person for a 3-day visa. The fee must be paid in cash (no credit cards) on arrival at the airport.

Visitors from countries with visa-on-arrival facility will have to go to a special counter to have their passports stamped with the on-arrival visa before going to the immigration clearance desk. The VOA visa is NOT EXTENDABLE OR RENEWABLE. A visa issued on arrival can be extended only in extraordinary circumstances such as natural disasters, accident, or illness. If you want to stay in Indonesia longer than the 30 days you must exit and re-enter the country on a new tourist visa.

Visa purchasing takes 15-30 minutes per applicant, depending on the number of persons applying. Payment counters, a bank counter, and a money changer have been set up to process payments. Passport must be valid for at least six months from the date of arrival. Payment must be made on arrival. An onward or return trip ticket must be shown on arrival.

      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Visa free facility
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        Visa free facility is granted to the citizens of 11 countries whose governments extend visa free facility to Indonesian nationals would continue to enjoy short visa-free stays. Included in the 11 countries and administrative districts that are granted the 30-day vis-free facility are: Brunei Darussalam, Chile, Hong Kong Special Administrative Region, Macau Special Administrative Region, Malaysia, Morocco, Peru, The Philippines, Singapore, Thailand, and Vietnam.

Visitors with the visa-free facility will be able to proceed directly to the immigration clearance counter after deplaning. Passport must be valid for a minimum of six months from the date of arrival. Onward or return tickets must be shown on arrival.

      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Citizens of other countries
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        Citizens of other countries not on the visa on arrival or visa free lists will be required to apply for a visa overseas (in their home country) before entering Indonesia. Citizens of any country wishing to stay more than 30 days must also apply for an appropriate visa (cultural visit or business) at their nearest Indonesian Embassy of Consulate before travelling to Indonesia.

Tour agents are able to arrange express handling for groups at no additional charge by presenting the completed immigration cards, passports and applicable visa fee. Passengers who overstay their visa period for a short period of time can be processed immediately at the airport by paying $20 every day they overstayed their 30-day visa. Airlines that experienced technical difficulties or delayed flights can apply for their passengers to be exempted from paying any overstay penalties.

Source : Ministry of Culture and Tourism, Republic of Indonesia

      </div>
      </div>
      </div>

  </div>

</div>