 <div class="container">
        <div class="countdown">
            <div class="row ">
                <div class="col-md-6 offset-md-3 quote ">
                    <div>
                    <h2 class="font-c"><b>Exhibitors Preview</b></h2>
                    <hr class="new2 text-align-center">
                </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="clr"></div>

    <div class="section-7  visitor  pt30 pb30 ">
        <div class="container"  >

<div class="container  ">
  <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/AceDrills-1.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>ACEDRILLS ROCK TOOLS CO.,LTD.</h4>
           <hr class="new4">
          </div>
          <div>Accredils Rock Tools Co., Ltd is a specialized manufacturer and supplier of Rocks
               Drilling Tools in China for 30 years, witch products covers a full range of mining 
               equipment, especially rocks drilling for mining, well drilling, construction, quarrying tunneling oil & gas industries etc.
          </div>
    </div>
  </div>
  <hr class="new5">

  <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/Anhui-NIngguo-1.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>ANHUI NINGGUO SHUNCHANG MACHINERY CO., LTD.</h4>
           <hr class="new4">
          </div>
          <div>Anhui Ningguo Shunchang Mechanics Co. Ltd. total area is 110,000 square meters. It’s total asset is RMB 80 million, and the registered capital is RMB 13 million. We start to R&D slewing ring in 2000, and then begin to produce formally slewing ring in batches in 2002, moreover, we did pass successfully ISO 9001 Quality System Authentication in 2003.
          </div>
    </div>
  </div>
  <hr class="new5">

    <div class="row">
    <div class="col-sm-2" >
         <!--img src="<?php //echo base_url() ?>assets/img/hemroc.png" width='175px' height='130px'"-->
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>APILE FOUNDATION EQUIPMENT CO., LTD. </h4>
           <hr class="new4">
          </div>
          <div>Apile Foundation Equipment Co., Ltd. is a high-tech enterprise, which is specialized and engaged in all kinds of piling rig,deep foundation drilling rig, mining exploration and mining and accessories, our products were covered foundation construction,deep foundation excavationg processing.
          </div>
    </div>
  </div>
  <hr class="new5">

    <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/Asme.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>ASSOCIATION OF SMALL & MEDIUM ENTERPRISES</h4>
           <hr class="new4">
          </div>
          <div>Anhui Ningguo Shunchang Mechanics Co. Ltd. total area is 110,000 square meters. It’s total asset is RMB 80 million, and the registered capital is RMB 13 million. We start to R&D slewing ring in 2000, and then begin to produce formally slewing ring in batches in 2002, moreover, we did pass successfully ISO 9001 Quality System Authentication in 2003.
          </div>
    </div>
  </div>
  <hr class="new5">

      <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/AXB.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>AXB TECHNOLOGY CO., LTD.</h4>
           <hr class="new4">
          </div>
          <div>AXB Technology Co., Ltd is invested by Japan Toyo (h.k) Machinery Co., Ltd. with registered capital USD 11 million. It covers an area of 36500㎡ with construction area of 44,000㎡. It is located at No.511 Yinhe Road Nanhu District, Jiaxing, Zhejiang province. 80km away from Shanghai and Hangzhou. AXB is specialized in manufacturing hydraulic breakers and spare parts. These products are widely applied to mines, railway & highway, municipal construction, electrical power & telecom construction and building demolition. AXB popular hydraulic breaker models: SB40, SB43, SB45, SB50, SB70, SB81, SB121, SB131, SB151.
          </div>
    </div>
  </div>
  <hr class="new5">

 <div class="row">
    <div class="col-sm-2" >
         <!--img src="<?php //echo base_url() ?>assets/img/hemroc.png" width='175px' height='130px'"-->
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>BEIJING CA-LONG ENGINEERING MACHINERY CO., LTD.</h4>
           <hr class="new4">
          </div>
          <div>Our main products include stationary Asphalt Mixing Plant (from 56 t/h to 600 t/h), Mobile Asphalt Mixing plant (from 56 t/h to 160 t/h), Concrete Mixing Plant (from 60 m3/h to 180 m3/h), Soil/Cement Mixing Plant and other machineries for roads infrastructure and maintenance. We have installed some Asphalt Mixing Plants in Indonesia, welcome to visit the site.
          </div>
    </div>
  </div>
  <hr class="new5">


    <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/Build-Chem-1.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>BUILDCHEM CO., LTD.</h4>
           <hr class="new4">
          </div>
          <div>BUILDCHEM CO., LTD. is an innovator focused on improving the quality and safety of floor coatings for industrial and commercial customers around the world. We are the developer of the innovative floor coating material HYBRID n-COAT. HYBRID n-COAT solves the weakness of former epoxy and Polyurethane coatings with properties such as superior abrasion resistance, never peel-off bonding structure, fire-resistance, acid /alkali resistance, stain resistance, etc. 
          </div>
    </div>
  </div>
  <hr class="new5">

      <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/Bossong.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>BUILDING TRENDS CO., LTD. (BOSSONG S.P.A)</h4>
           <hr class="new4">
          </div>
          <div>BOSSONG S.P.A., a former German company, is an Italian manufacturer of fastening systems since 1962. We are deeply specialized in chemical anchors (with pure epoxy as our strongest product) for anchoring and post-installed rebar connections, heavy duty fixing and powder actuated fixing.
          </div>
    </div>
  </div>
  <hr class="new5">

        <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/Changzhou.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>CANGZHOU HENGJIA PIPELINE CO., LTD.</h4>
           <hr class="new4">
          </div>
          <div>BCangzhou HENGJIA pipeline Co., Ltd., Main products are pipe fittings(elbows, tees, reducers, caps,etc.), flanges, valve,steel pipes and other products such as structural steel etc. that are supplied to power plants, shipping industry, chemical industry and other industries home and abroad. Material including: stainless steel, alloy steel, carbon steel, etc. HENGJIA Committed to providing customers one-stop service.
          </div>
    </div>
  </div>
  <hr class="new5">


        <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/Changzhou-Rong-Yu-1.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>CHANGZHOU DIESEL IMP&EXP CO., LTD.</h4>
           <hr class="new4">
          </div>
          <div>CHANGZHOU DIESEL IMP&EXP CO., LTD, Jiangsu province, China (the former, Changzhou Peiyi Engineering and Machinery Parts Co., Ltd) was founded in 2006. At beginning of its development, DIESEL IMP&EXP was curious about and will to perceive everything around like a baby. The growth of a baby needs a favorable environment and atmosphere, so some people will try to guide you to discover everything in this society.
          </div>
    </div>
  </div>
  <hr class="new5">


   <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/Changzhou-Kaipeng-1.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>CHANGZHOU KAIPENG NEW ENERGY TECHNOLOGY CO., LTD.</h4>
           <hr class="new4">
          </div>
          <div>Founded in 1986, Changzhou Kaipeng Hydro-Equipment Co., Ltd (formerly known as Changzhou Kaitong Hydro-Equipment Factory) meets the severe market challenges with a new image, adheres to the concept of “Innovation-powered, Market-oriented” and the attitude of “Concentration, Professionalism, and Specialization” and strives for the healthy and sustainable development of “KT” brand oil cooler and “Kaitong” brand oil tank accessories.
          </div>
    </div>
  </div>
  <hr class="new5">


     <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/Chengdu-Gute.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>CHENGDU GUTE MACHINERY WORKS CO., LTD.</h4>
           <hr class="new4">
          </div>
          <div>GUTE-small and medium construction machinery manufacturing experts,established in 1994 has focused on the research,manufacture and marketing service for 24years.We have more than 1 million Gute machines for serving construction sectors in 71 countries and regions.
          </div>
    </div>
  </div>
  <hr class="new5">


     <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/DIC-Trading-1.png" width='175px' height='50px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>DIC TRADING COMPANY</h4>
           <hr class="new4">
          </div>
          <div>DIC TRADING was established in 2017, We dealing Flutek Korea’s Hydraulic pump, motor and parts for Doosan, Hyundai and Volvo (K3V/K5V SERIES PUMP, SWING MOTOR, REDUCTION GEAR and related parts). We provide the most competitive in the world to our customer’s construction equipment with excellent durability and control ability. We will do our best to gives the highest satisfaction. Thank you very much!
          </div>
    </div>
  </div>
  <hr class="new5">


     <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/DOA-1.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>DO-A INDUSTRIAL CO., LTD.</h4>
           <hr class="new4">
          </div>
          <div>With a top priority given to customer satisfaction and quality management by virtueof future-oriented technologies, Do-A Industrial Co., faithful to the basics, principles andcorporate ethics, has grown to become a company committed to creating beautiful street culture which goes well with surrounding environment and functions.Since establishment in 1983, we have successfully carried out construction of lots ofmetal structures and windows at home as well overseas.
          </div>
    </div>
  </div>
  <hr class="new5">


       <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/Elabram.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>ELABRAM SYSTEMS PTE. LTD.</h4>
           <hr class="new4">
          </div>
          <div>Elabram Systems is a reliable Telecommunication Engineering and IT service provider specialised in Manpower Outsourcing, Project Management and Engineering Services of advance telecommunication in wireless network solutions. Our collective experience and expertise gained from working with many global clients in telecommunications equipment manufacturing and telecommunications operations, has enabled us to source for, and provide, specialist resources to all our other clients. Elabram Services are Manpower Solutions Outsourcing (MSO), Network Planning Optimization (NPO), Workforce Management Systems (WMS), E-Pro and Software Solutions.
          </div>
    </div>
  </div>
  <hr class="new5">

       <div class="row">
    <div class="col-sm-2" >
         <img src="<?php echo base_url() ?>assets/img/Fairmont.png" width='175px' height='130px'">
    </div>
      <div class="col-sm-10" >
          <div> 
           <h4>FAIRMONT INDUSTRIES SDN BHD</h4>
           <hr class="new4">
          </div>
          <div>The manufacturing operations of Fairmont are based in Malaysia and we are an ISO: 9001 certified manufacturer. We commenced manufacturing since 1998 and have accumulated a track record of more than 18 years manufacturing experience. Guided by this principle, we produce and distribute a wide range of EPDM rubber granules and PU products under our Ecolastic® and Airethane brand. Fairmont supplies to domestic and international markets across North America, Europe, Asia and Australia. 
          </div>
    </div>
  </div>
  <hr class="new5">

</div>
        
     
        </div>
        </div>
    </div>
    
  