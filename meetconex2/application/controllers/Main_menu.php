<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_menu extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->model('Menu_model');
		
	}

	public function index()
	{
		$this->load->template('menu/index');
	}

		public function exhibith_encuiries()
	{
		$this->load->template('menu/exibith-encuiries');
	}

		public function registration()
	{
		$this->load->template('menu/FormRegistration');
	}
	public function registrationQuiries()
	{
		$this->load->template('menu/Registration');
	}


	/*Contact Organizer*/
		public function contact_organizer()
	{
			/*$data['menu'] = $this->menu_m->tampil();*/
			$data['title']='contact organizers';
			$data['contact'] = $this->Menu_model->list_contact()->result();
		$this->load->template('menu/ContactOrganizer',$data);
	}
	/*Contact Organizer*/

		public function AboutTheShow()
	{
		$this->load->template('menu/AboutTheShow');
	}

		public function Visitor_Profile()
	{
		$this->load->template('menu/visitor-profile');
	}

		public function Visa_Information()
	{
		$this->load->template('menu/visa-information');
	}
		public function Why_exhibit()
	{
		$this->load->template('menu/why-exhibit');
	}

		public function official_hotel()
	{
		$this->load->template('menu/official-hotel');
	}
	public function exhibitors_preview()
	{
		$this->load->template('menu/exhibitors-preview');
	}
	  
}
