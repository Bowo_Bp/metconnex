    <div class="clr"></div>
    <div class="section-3 pt30 pb30 text-align-center">
        <div class="container">
       
 <div class="form">
        
        <form method="post" action="<?php echo base_url('Transaction/create');?>">
                <div class="form-content">
                    <div class="row">

                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Title *" value="" name="salutation">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name *" value="" name="firstName"/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name *" value="" name="lastName"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Job Title *" value="" name="job"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name *" value="" name="company"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Street Address *" value="" name="street"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address Line 2 *" value="" name="address2"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City *" value="" name="city"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="State / Province / Region *" value="" name="state"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Zip / Postal Code *" value="" name="zipCode"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Country *" value="" name="country"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email *" value="" name="eMail"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone Number *" value="" name="phone"/>
                            </div>
                        </div> 
                        <div class="col-md-12">
                           <p align="left"> i am spesifically interested in finding out more about</p>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Exhibiting" name="chkForm[]"> Exhibiting
                            </label>
                        </div>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Sponsoship" name="chkForm[]"> Sponsoship
                            </label>
                        </div>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Other" name="chkForm[]"> Other
                            </label>
                        </div>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Event Partnership" name="chkForm[]"> Event Partnership
                            </label>
                        </div>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Media Partnership" name="chkForm[]"> Media Partnership
                            </label>
                        </div>
                        </div>
                        <div class="col-md-12">
                            <p align="left">Which type of companies or people are you looking to discuss business with?</p>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="" value="" name="desc"/>
                               <p align="left"> All information collected during this registration prosess is subject to our general Privacy policy which can be consulted here.</p>
                            </div>
                        </div> 
                        <div class="col-md-12">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox"> I have read, understood and consent to your Privacy policy.
                            </label>
                        </div>
                        </div>
                    <input type="text" value="<?php echo (new \DateTime())->format('YmdHisms'); ?>" name="vqr" hidden="hidden">
                    </div>
                    <button type="submit" class="btnSubmit">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    
   