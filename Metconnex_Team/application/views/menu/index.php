    <div class="container">
        <div class="countdown">
            <div class="row">
                <div class="col-md-6 offset-md-3 quote">
                    “Metallurgy is the art of extracting metals from their ores and adapting them to various purposes of manufacture”
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-md-3 timer">
                    <ul>
                        <li><span class="timer-title">Days</span><span id="days"></span></li>
                        <li><span class="timer-title">Hours</span><span id="hours"></span></li>
                        <li><span class="timer-title">Minutes</span><span id="minutes"></span></li>
                        <li><span class="timer-title">Seconds</span><span id="seconds"></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="section-2">
        <div class="sec-left">
            <div class="sec-title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 offset-md-3">
                            <h6>Conference: 7 Aug – 8 Aug 2019 | Exhibition : 7 Aug – 9 Aug 2019</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container pt30">
                <div class="row justify-content-start">
                    <div class="col-md-9 offset-md-3">
                        <h3>ABOUT IAMET ITB</h3>
                        <p>Ikatan Alumni Teknik Metalurgi (IA Met) ITB, as one of the stakeholders of the mining and mineral processing industry, that has the mission to be the facilitator of development of metallurgical industry in the country, understand the challenges and have also the responsible to contribute on the successful implementation of the regulations. One of the contributions that IA Met can do is facilitating all related stakeholders to gather in a metallurgical processing-focused exhibition and international seminar that have never been held before.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="sec-right">
            <div class="sec-title">
                <div class="container">
                    <div class="row justify-content-start">
                        <div class="col-md-9">
                            <h6>Jakarta Convention Centre, Senayan Jakarta</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container pt30">
                <div class="row justify-content-start">
                    <div class="col-md-9">
                        <h3>CONFERENCE</h3>
                        <p>The conference will be held adjacent to the exhibition area from 7 – 8 August 2019. The conference aims to bring together leading academic, researchers, regulators and industry as
                            well as the technology provider to exchange and share their experience and research result on all aspect of extractive metallurgy. It also provides the opportunities of policy makers,
                            top managers, researchers, practitioners and educators to present and discuss the most recent innovation, trends, and concerns as well as practical challenges in the field of extractive metallurgy.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clr"></div>
    <div class="section-3 pt30 pb30 text-align-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="font-white pb30">BOOK YOUR SPACE NOW !!!</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="box-option">
                        <div class="option-img"><img src="<?php base_url() ?>assets/img/option-1.png"></div>
                        <div class="clr"></div>
                        <div class="box-title">Option 1</div>
                        <p>Space Only</p>
                        <h5>@ USD 300 / M</h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-option">
                        <div class="option-img"><img src="<?php base_url() ?>assets/img/option-2.png"></div>
                        <div class="clr"></div>
                        <div class="box-title">Option 2</div>
                        <p>Standart Shell Scheme</p>
                        <h5>@ USD 350 / M</h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-option">
                        <div class="option-img"><img src="<?php base_url() ?>assets/img/option-3.png"></div>
                        <div class="clr"></div>
                        <div class="box-title">Option 3</div>
                        <p>9 Sqm – Upgraded Standard Shell Scheme</p>
                        <h5>@ USD 3250</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-4 pt30 pb30">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="exibith-img"><img src="<?php base_url() ?>assets/img/exibith.png"></div>
                </div>
                <div class="col-md-6 font-white exibith-text">
                    <h5>EXHIBITION</h5>
                    <p>The exhibition will be showcasing the related stakeholders from the mineral and metal processing industry both local and international. Exhibitors will have a chance to promote their solution for the metallurgical industry.</p>
                </div>
            </div>
        </div>
    </div>
    