<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_menu extends CI_Controller {


	public function index()
	{
		$this->load->template('menu/index');
	}

		public function exhibith_encuiries()
	{
		$this->load->template('menu/exibith-encuiries');
	}

		public function registration()
	{
		$this->load->template('menu/FormRegistration');
	}

		public function contact_organizer()
	{
		$this->load->template('menu/ContactOrganizer');
	}

		public function AboutTheShow()
	{
		$this->load->template('menu/AboutTheShow');
	}

		public function Visitor_Profile()
	{
		$this->load->template('menu/visitor-profile');
	}

			public function Visa_Information()
	{
		$this->load->template('menu/visa-information');
	}
}
