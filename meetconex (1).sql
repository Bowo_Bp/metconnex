-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Apr 2019 pada 22.03
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meetconex`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `contact_organizers`
--

CREATE TABLE `contact_organizers` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `email` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `contact_organizers`
--

INSERT INTO `contact_organizers` (`id`, `name`, `phone`, `email`) VALUES
(1, 'Rizky', '083804956845', 'rizky.pancaran346@gmail.com'),
(2, 'Udin', '088493948', 'rizky123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `exhibitors_preview`
--

CREATE TABLE `exhibitors_preview` (
  `id` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` text NOT NULL,
  `gambar` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `menu_utama` varchar(60) NOT NULL,
  `url` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `menu_utama`, `url`) VALUES
(1, 'dashboard', 'home'),
(2, 'artikel', 'home/artikel'),
(3, 'kategori', 'home/kategori'),
(4, 'agenda', 'home/agenda'),
(5, 'download', 'home/download'),
(6, 'galerry', 'home/galerry'),
(7, 'menu', 'home/main_menu'),
(8, 'user', 'home/user'),
(9, 'general', 'home/general'),
(10, 'tes', 'home/tes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `submenu_detail`
--

CREATE TABLE `submenu_detail` (
  `id` int(11) NOT NULL,
  `submenu_id` int(11) NOT NULL,
  `submenu_det` varchar(60) NOT NULL,
  `sublink` varchar(60) NOT NULL,
  `frontend_link` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `submenu_detail`
--

INSERT INTO `submenu_detail` (`id`, `submenu_id`, `submenu_det`, `sublink`, `frontend_link`) VALUES
(2, 10, 'About IAMET ITB', '', 'Main_menu/about_iameet_itb'),
(3, 10, 'About The Show', '', 'Main_menu/about_theshow'),
(4, 10, 'Dates & Venue', '', 'Main_menu/date_vanue'),
(5, 11, 'Why	Visit?', '', 'Main_menu/why_visit'),
(6, 11, 'Show Features', '', ''),
(7, 11, 'Conference', '', ''),
(8, 11, ' Exhibitors Preview', '', 'Main_menu/exhibitors_preview'),
(9, 11, ' EPlan Your Visit', '', ''),
(10, 11, ' Dates & Venue', '', ''),
(11, 11, ' Visitor Profile', '', 'Main_menu/Visitor_Profile'),
(12, 11, ' Official Hotel', '', 'Main_menu/official_hotel'),
(13, 11, ' Visa Information', '', 'Main_menu/Visa_Information'),
(14, 11, ' Free Visitor	Registration', '', ''),
(15, 12, ' Agenda', '', ''),
(16, 12, ' Speaker Profile', '', ''),
(17, 12, ' Delegate	Registration', '', ''),
(18, 13, 'Why Exhibit', '', 'Main_menu/why_exhibit'),
(19, 13, 'Exhibitor	Enquiries', '', 'Main_menu/exhibith_encuiries'),
(20, 13, 'Exhibit Profile', '', ''),
(21, 13, 'Sales	Brochure', '', ''),
(22, 14, 'Why Sponsor?', '', 'Main_menu/why_sponsor'),
(23, 14, 'Sponsorship Opportunity', '', ''),
(24, 14, 'Sponsorship Enquiry', '', ''),
(25, 14, 'Download Sponsorship Brochure', '', ''),
(26, 15, 'Press Corner', '', ''),
(27, 15, 'Latest	News', '', ''),
(28, 15, 'Gallery', '', ''),
(33, 16, 'Enquiries', '', ''),
(35, 16, 'Contact The Organisers', '', 'Main_menu/contact_organizer'),
(36, 16, 'Enquiries', '#', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_menu`
--

CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `submenu` varchar(60) NOT NULL,
  `submenu_link` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_menu`
--

INSERT INTO `sub_menu` (`id`, `menu_id`, `submenu`, `submenu_link`) VALUES
(1, 2, 'Semua Artikel', 'home/artikel_list'),
(2, 2, 'Tambah Artikel', 'home/tambah_artikel'),
(3, 3, 'Semua Kategori', 'home/semua_kategori'),
(4, 4, 'Tambah Kategori', 'home/tambah_kategori'),
(5, 4, 'Semua Agenda', 'home/tambah_agenda'),
(6, 5, 'list_download', 'home/list_download'),
(7, 6, 'List Gallery', 'home/list_gallery'),
(8, 5, 'Tambah File', 'home/tambah_file'),
(9, 7, 'Home', 'home/home_form'),
(10, 7, 'Show Info', 'home/info_form'),
(11, 7, 'Visit', 'home/visit_form'),
(12, 7, 'Conference', 'home/conference_form'),
(13, 7, 'Exhibit', 'home/exhibit_form'),
(14, 7, 'Sponsor', 'home/sponsor_form'),
(15, 7, 'Media', 'home/media_form'),
(16, 7, 'Contact', 'home/contact_list');

-- --------------------------------------------------------

--
-- Struktur dari tabel `visitor_profile`
--

CREATE TABLE `visitor_profile` (
  `id` int(11) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `contact_organizers`
--
ALTER TABLE `contact_organizers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `exhibitors_preview`
--
ALTER TABLE `exhibitors_preview`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `submenu_detail`
--
ALTER TABLE `submenu_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `visitor_profile`
--
ALTER TABLE `visitor_profile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `contact_organizers`
--
ALTER TABLE `contact_organizers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `exhibitors_preview`
--
ALTER TABLE `exhibitors_preview`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `submenu_detail`
--
ALTER TABLE `submenu_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `sub_menu`
--
ALTER TABLE `sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `visitor_profile`
--
ALTER TABLE `visitor_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
