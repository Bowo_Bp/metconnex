<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {

	public $transactionCRUD;

		public function __construct() {
			parent::__construct(); 

			// $this->load->library('form_validation');
			// $this->load->library('session');
			$this->load->model('Transaction_Model');

			$this->transactionCRUD = new Transaction_Model;
		}

		public function create()
		{
			$this->transactionCRUD->insert_txnForm();
			redirect('Main_menu/index');
		}
}
?>