<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Met Connect</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font.css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
    function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

    
</head>
<body>
<div class="section-1">
    <div class="container header-home">
        <div class="row">
            <div class="col-md-3 logo-top"><a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>assets/img/logo.jpg"></a></div>
            <div class="col-md-7 tgl-top">
                <h3>7 AUG 2019 - 9 AUG 2019</h3>
                <p>Jakarta Convention Centre, Senayan Jakarta</p>
            </div>
            <div class="col-md-2 pt30">
                <a class="btn-orange-1" href="<?php echo base_url() ?>Main_menu/registrationQuiries" >Registration</a>
            </div>
        </div>
    </div>

<!-- Navbar -->


<!-- End Navbar-->
    <div class="nav-top">
        <div class="container">
            <div class="col-md-12">
                <ul class="main-menu">
                    <li class="active"><a href="<?php echo base_url() ?>">Home</a></li>
                    <li class="dropdown"><a href="">About</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url()?>Main_menu/AboutTheShow">About The Show</a></li>
                            <li><a href="<?php echo base_url()?>Main_menu/TheVenue">The Venue</a></li>
                        </ul>
                    </li>

                    <li class="dropdown"><a href="">Visiting</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url()?>Main_menu/WhyVisit">Why Visit ?</a></li>
                            <li><a href="">Show Features</a></li>
                            <li><a href="<?php echo base_url()?>Main_menu/exhibitors_preview">Exhibitors Preview</a></li>
                            <li><a href="">Where & When</a></li>
                            <li><a href="<?php echo base_url()?>Main_menu/Visitor_Profile">Visitor Profile</a></li>
                            <li><a href="<?php echo base_url()?>Main_menu/official_hotel">Official Hotel</a></li>
                            <li><a href="<?php echo base_url()?>Main_menu/Visa_Information">Visa Information</a></li>
                            <li><a href="">Free Visitor Registration</a></li>
                        </ul>
                    </li>

                    <li class="dropdown"><a href="">Exhibiting</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url()?>Main_menu/why_exhibit">Why Exhibit</a></li>
                            <li><a href="<?php echo base_url()?>Main_menu/exhibith_encuiries">Exhibitor & Sponsorship Enquiries</a></li>
                            <li><a href="">Exhibit Profile</a></li>
                            <li><a href="">Sales Brochure</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="">Supporters</a>
                        <ul class="dropdown-menu">
                            <li><a href="">Media Partners</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="">News</a>
                        <ul class="dropdown-menu">
                            <li><a href="">Latest News</a></li>
                            <li><a href="">Gallery</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="">Contact</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url() ?>Main_menu/contact_organizer">Contact The Organizers</a></li>
                        <li><a href="">Enquiries</a></li>
                    </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
