    <div class="container">
        <div class="countdown">
            <div class="row">
                <div class="col-md-6 offset-md-3 quote">
                   <h3>About The Show</h3>
                   
                        <hr class="new1">
                
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="clr"></div>
    <div class="AboutTheShow pt30 pb30 text-align-center">

 <div class="row">
        <div class=" text-align-right col-md-2">
            
            </div>
        <div class=" text-align-right col-md-5">
            <!-- <h3 align="left">Industry Over View</h3> -->
                <!-- <hr class="new3"> -->
                <h3>
                <p align="justify">
                <b> Construction Indonesia, now in its 20th edition is Indonesia’s largest and most popular construction exhibition, held at Jakarta International Expo
                </p> </h3>
                <br>
                <p align="justify">
                <i>With huge investments ongoing in the region Construction Indonesia provides the perfect platform for industry professionals and key players from the global construction industry to network and showcase their latest products and services under one roof.
                </p>
                <hr class="new3">
     </div>
     <div class=" text-align-right col-md-5 ">
     <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-500" src="<?php base_url() ?> assets/img/AboutTheShow1.png" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-500" src="<?php base_url() ?> assets/img/AboutTheShow2.png" alt="Second slide">
    </div>
    
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

     </div>
               
            </div>
           
    </div>
    
   